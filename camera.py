# -*- coding: utf-8 -*-
import Tkinter
import tkFileDialog
import movieDetect
import cv2
import glob
import os
from datetime import datetime, timedelta
from common import draw_str, image_extensions

print('ESC - Exit\nSPACE - Photo\nENTER - Video')


# Расширение файла
def get_extension(path):
    file_name = path.rfind('.')
    if file_name > 0:
        file_name = path[file_name:]
        return file_name


def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30),
                                     flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:, 2:] += rects[:, :2]
    return rects


def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)


def make_filename(basename, ext, check_path=None):
    path = ''
    if check_path:
        dirs = os.listdir(check_path)
        path = check_path + '/'
    else:
        dirs = os.listdir(os.getcwd())
    ind = 1
    while True:
        name = basename + '_' + str(ind) + ext
        if name not in dirs:
            return path + name
        ind += 1


class NButton(Tkinter.Button):
    def __init__(self, *args, **kw):
        Tkinter.Button.__init__(self, *args, width=20, height=2, **kw)


class Main(Tkinter.Tk):
    program_name = 'CameraPy'

    def __init__(self):
        Tkinter.Tk.__init__(self)
        self.title(self.program_name)
        self.resizable(False, False)
        self.interval = Tkinter.IntVar()
        self.func = Tkinter.IntVar()

        self.frame = None
        self.path = None
        self.files = []
        # self.types = ['*.png', '*.jpg', '*.jpeg', '*.bmp']
        self.types = image_extensions
        self.ratio = (640, 480)
        self.video_out = None
        self.cascade = cv2.CascadeClassifier("data/haarcascades/haarcascade_frontalface_alt.xml")
        self.nested = cv2.CascadeClassifier("data/haarcascades/haarcascade_eye.xml")

        m = Tkinter.Menu(self)
        self.iconbitmap('Icon.ico')
        self.config(menu=m)

        func_menu = Tkinter.Menu(m)
        cam_menu = Tkinter.Menu(func_menu)
        file_menu = Tkinter.Menu(func_menu)
        dir_menu = Tkinter.Menu(func_menu)
        m.add_cascade(label="Функции", menu=func_menu)
        func_menu.add_cascade(label="Камера", menu=cam_menu)
        func_menu.add_cascade(label="Файлы", menu=file_menu)
        func_menu.add_cascade(label="Папка", menu=dir_menu)

        cam_menu.add_radiobutton(label="Без эффекта", value=0, variable=self.func)
        cam_menu.add_radiobutton(label="Выделить лицо", value=1, variable=self.func)
        cam_menu.add_radiobutton(label="Вырезать лицо", value=2, variable=self.func)
        file_menu.add_command(label="Выделить лицо", command=self.file_face_detect)
        file_menu.add_command(label="Вырезать лицо", command=self.file_cut_face)
        dir_menu.add_command(label="Выделить лицо", command=self.dir_face_detect)
        dir_menu.add_command(label="Вырезать лицо", command=self.dir_cut_face)

        setting_menu = Tkinter.Menu(m)
        interval_menu = Tkinter.Menu(m)
        m.add_cascade(label="Настройки", menu=setting_menu)
        setting_menu.add_cascade(label="Интервал сохранения", menu=interval_menu)
        interval_menu.add_radiobutton(label="Без интервала", value=0, variable=self.interval)
        interval_menu.add_radiobutton(label="5 секунд", value=5, variable=self.interval)
        interval_menu.add_radiobutton(label="10 секунд", value=10, variable=self.interval)
        interval_menu.add_radiobutton(label="15 секунд", value=15, variable=self.interval)
        setting_menu.add_command(label="Выбрать путь сохранения", command=self.change_path)

        NButton(self, text="Включить камеру", command=self.write).grid(row=1, column=0)
        NButton(self, text="Детектор движений", command=movieDetect.main).grid(row=1, column=1)

    # Включить вебку на запись
    def write(self):
        img_clock = None
        video_clock = None
        is_record = False
        cam = cv2.VideoCapture(0)
        # print('Width: ', cam.get(3), ' Height: ', cam.get(4))
        # width = cam.set(3, 1280)
        # height = cam.set(4, 720)
        video_name = make_filename(self.program_name, '.avi', check_path=self.path)
        while True:
            _, self.frame = cam.read()
            # print_time(frame)
            if self.func.get() == 1:
                self.frame = self.face_detect(self.frame)
            elif self.func.get() == 2:
                self.frame = self.cute_face(self.frame)
            if img_clock and (img_clock - datetime.now()).seconds > 0:
                img_text = 'Photo: ' + str((img_clock - datetime.now()).seconds)
                self.print_time(self.frame, img_text)
            if video_clock and (video_clock - datetime.now()).seconds > 0:
                video_text = 'Video: ' + str((video_clock - datetime.now()).seconds)
                self.print_time(self.frame, video_text, height=50)
            cv2.imshow('WebCam', self.frame)
            if is_record:
                self.video_out.write(self.frame)
            ch = cv2.waitKey(33)
            if ch == 27:
                cam.release()
                if self.video_out:
                    self.video_out.release()
                    print('Record End', video_name)
                cv2.destroyAllWindows()
                break
            if ch == 32:
                if self.interval.get() == 0:
                    self.save_img(self.frame, self.program_name)
                else:
                    img_clock = datetime.now() + timedelta(seconds=self.interval.get())
            if ch == 13:
                if self.interval.get() == 0:
                    is_record = True
                    self.video_out = cv2.VideoWriter(video_name, -1, 1, self.ratio)
                    print('Record ...')
                else:
                    video_clock = datetime.now() + timedelta(seconds=self.interval.get())
            if img_clock and img_clock < datetime.now():
                img_clock = None
                self.save_img(self.frame, self.program_name)
            if video_clock and video_clock < datetime.now():
                video_clock = None
                is_record = True
                self.video_out = cv2.VideoWriter(video_name, -1, 1, self.ratio)
                print('Record ...')

    def change_path(self):
        self.path = tkFileDialog.askdirectory()
        if len(self.path) == 0:
            self.path = None

    def open_files(self):
        self.files = tkFileDialog.askopenfilenames(filetypes=[('images', ' '.join(self.types))])
        print 'Selected Files: ', self.files

    def open_images(self):
        path = tkFileDialog.askdirectory()
        if len(path) == 0:
            return
        self.files = []
        print('Upload Files ...')
        for item in self.types:
            interim_list = []
            interim_full = []
            depth = '/'
            item = '*' + item
            while 1:
                if len(glob.glob(path + depth + '*')) > 0:
                    interim_list = glob.glob(path + depth + item)
                    print path + depth + item
                    depth += '*/'
                    if len(interim_list) > 0:
                        interim_full.extend(interim_list)
                else:
                    if len(interim_full) > 0:
                        self.files.append(interim_full)
                    break
            print 'Found: ', item, len(interim_full)

    # Открыть картинку
    def open_image(self, image):
        try:
            img = cv2.imread(image)
        except UnicodeEncodeError:
            img = cv2.imread(image.encode('cp1251'))
        if img is None:
            print 'Not Support: ', image
            return
        return img

    def save_img(self, frame, name, ext='.png'):
        sa = make_filename(name, ext, check_path=self.path)
        try:
            cv2.imwrite(sa, frame)
        except UnicodeEncodeError:
            cv2.imwrite(sa.encode('cp1251'), frame)
        print 'Save Image: ', sa
        return sa

    def print_time(self, frame, text, width=20, height=20):
        draw_str(frame, (width, height), text)
        return frame

    def face_detect(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)
        rects = detect(gray, self.cascade)
        draw_rects(frame, rects, (0, 255, 0))
        if not self.nested.empty():
            for x1, y1, x2, y2 in rects:
                roi = gray[y1:y2, x1:x2]
                vis_roi = frame[y1:y2, x1:x2]
                subrects = detect(roi.copy(), self.nested)
                draw_rects(vis_roi, subrects, (255, 0, 0))
        return frame

    def file_face_detect(self):
        self.open_files()
        for item in self.files:
            frame = self.open_image(item)
            frame = self.face_detect(frame)
            ext = get_extension(item)
            self.save_img(frame, 'FaceDetect', ext=ext)

    def dir_face_detect(self):
        self.open_images()
        for item in self.files:
            for image in item:
                frame = self.open_image(image)
                frame = self.face_detect(frame)
                ext = get_extension(image)
                self.save_img(frame, 'FaceDetect', ext=ext)

    def cute_face(self, frame):
        rects = detect(frame, self.cascade)
        if len(rects) > 0:
            frame = frame[rects[0][0]:rects[0][2], rects[0][1]:rects[0][3]]
        return frame

    def file_cut_face(self):
        self.open_files()
        for item in self.files:
            frame = self.open_image(item)
            frame = self.cute_face(frame)
            ext = get_extension(item)
            self.save_img(frame, 'CuteFace', ext=ext)

    def dir_cut_face(self):
        self.open_images()
        for item in self.files:
            for image in item:
                frame = self.open_image(image)
                frame = self.cute_face(frame)
                ext = get_extension(image)
                self.save_img(frame, 'CuteFace', ext=ext)

Main().mainloop()
